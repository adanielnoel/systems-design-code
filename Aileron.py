def q_inf(rho, velocity):
    q_inf = 0.5 * rho * velocity ** 2

    return q_inf


#################################################################################
# INTEGRATION FOR ROLLING MOMENT COEFFICIENT                                  ###
#################################################################################
def roll_coefficient(b1, b2, cl_a, S_ref, taper, b_span, aileron_over_chord, slices_n, rho, velocity):
    ######################################################
    # FUNCTION DEFINITIONS                              ##
    ######################################################

    k = 1 - float(aileron_over_chord)

    def C(span_b, area_s, taper, y):  # C(y) CHORD LENGTH AS A FUNCTION OF Y
        semi_b = span_b / 2.0
        semi_s = area_s / 2.0
        return (2 * semi_s) / ((taper + 1) * semi_b) - y * (semi_b ** 2) * (taper + 1) / (2 * semi_s * (taper - 1))

    def aileron_effectiveness(k):  # AILERON EFFECTIVENESS (TAU)

        x = 1 - float(k)

        x_4 = -  6.0417 * x ** 4

        x_3 = 11.191 * x ** 3

        x_2 = -  7.8335 * x ** 2

        x_1 = 3.2117 * x

        x_0 = 0.0015

        y = x_0 + x_1 + x_2 + x_3 + x_4

        return y

    def interval_slice(limit1, limit2, slices):
        interval = float(limit2) - float(limit1)

        interval_slice = interval / slices

        return interval_slice

    # tau = float(aileron_effectiveness(k))
    #
    # const_integration = (2 * cl_a * tau) / (S_ref * b_span)
    #
    # ## Guess what, integral is gone!
    #
    # aileron_eff_area = (b2 - b1) * C(b_span, S_ref, taper, b2 / 2.0) + C(b_span, S_ref, taper, b1 / 2.0) / 2
    # centroid_aileron_eff_area = b1/2.0 + (b2 - b1) * (
    #     2 * C(b_span, S_ref, taper, b2 / 2.0) + C(b_span, S_ref, taper, b1 / 2.0)) / \
    #                             (6 * (C(b_span, S_ref, taper, b2 / 2.0) + C(b_span, S_ref, taper, b1 / 2.0)))
    #
    # return const_integration * centroid_aileron_eff_area * aileron_eff_area
    dy = interval_slice(b1, b2, slices_n)

    y = b1 - dy

    integral = float(0)

    tau = float(aileron_effectiveness(k))

    const_integration = (2 * cl_a * tau) / (S_ref * b_span)

    #####################################################
    #####    MAIN FUNCTION INTEGRATING LOOP   ###########
    #####################################################

    while y < b2:
        y = y + dy

        slice = C(b_span, S_ref, taper, y) * y * dy

        integral = integral + slice

    return const_integration * integral


#################################################################################
# INTEGRATION FOR DAMPING COEFFICIENT                                         ###
#################################################################################
def damping_coefficient(cl_a, cd_0, S_ref, taper, b_span, slices_n):
    def C(span_b, area_s, taper, y):  # C(y) CHORD LENGTH AS A FUNCTION OF Y

        C_r = (2 * float(area_s)) / (float(span_b) * (1 + float(taper)))

        m_grad = C_r * (float(taper) - 1) / (span_b / 2)

        c_fy = C_r + y * m_grad

        return c_fy

    def interval_slice(limit1, limit2, slices):  # Defining dy based on slices_n

        interval = float(limit2) - float(limit1)

        interval_slice = interval / slices

        return interval_slice

    dy = interval_slice(0, (b_span / 2), slices_n)

    y = 0 - dy

    integral = float(0)

    const_integration = (4 * (cl_a + cd_0)) / (float(S_ref) * b_span)

    while y < (b_span / 2):
        y = y + dy

        slice = y ** 2 * C(b_span, S_ref, taper, y) * dy

        integral = integral + slice

    return const_integration * integral


# def roll_coefficient(b1, b2, cl_a, S_ref, taper, b_span, aileron_over_chord, dy_slice, deflection_aileron, rho, velocity):
#
# rc = roll_coefficient(15.0, 15.8, 0.110, 90.0, 0.35, 32, 0.3, 200, 1.225, 200.0)
#
# dc = damping_coefficient(0.110, 0.002, 90.0, 0.35, 24.0, 200)
#
# print rc,dc
#
# print rc/dc*20*(2*200/32)


def C(span_b, area_s, taper, y):  # C(y) CHORD LENGTH AS A FUNCTION OF Y

    C_r = (2 * float(area_s)) / (float(span_b) * (1 + float(taper)))

    m_grad = C_r * (float(taper) - 1) / (span_b / 2)

    c_fy = C_r + y * m_grad

    return c_fy

################################################################
# Temp trash
################################################################
# def D_LE(c_y, k, d_aileron_rad):
#
#     d_le = c_y * (1 - k) * math.tan(d_aileron_rad)
#
#     return d_le
#
################################################################
# def D_AOA_W(d_le, c_y):
#
#     d_aoa_w = math.atan(d_le / c_y)
#
#     return d_aoa_w
#
################################################################
