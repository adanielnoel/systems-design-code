from Aileron import *
from math import fabs

############### Preamble_start
b1 = 0
d_aileron = 0
P = 0
############### Preamble_end

##################################
# PARAMETERS OF AIRCRAFT        ##
##################################
                                ##
# Roll requirement              ##
roll_req = 45                   ##
time_req = 1.4                  ##
P_req = roll_req / time_req     ##
                                ##
# Planform parameters           ##
S_ref = 96.95                   ##
taper = 0.35                    ##
b_span = 28.1                   ##
aileron_over_chord = 0.3        ##
                                ##
# Aerodynamic parameters        ##
cl_a = 0.100                    ##
cd_0 = 0.00383                  ##
                                ##
# Flight condition parameters   ##
rho = 1.225                     ##
V = 45                          ##
                                ##
# Optimisation integral slices  ##
slices_n = 200                  ##
##################################


# Roll rate function with respect to undetermined / changeable variables
def P_val(b1, b2, d_aileron):
    rc = roll_coefficient(b1, b2,
                          cl_a, S_ref, taper, b_span, aileron_over_chord,
                          slices_n,
                          rho, V)

    dc = damping_coefficient(cl_a, cd_0,
                             S_ref, taper, b_span,
                             slices_n)

    P = -(rc / dc) * d_aileron * (2.0 * V / float(b_span))

    return P

print C(28.1, 96.95, 0.35, 0)
###################################################################################################
# Optimisation type                                                                              ##
###################################################################################################
# type 1 is reiteration to determine aileron span b2-b1 given b2 and aileron deflection
# type 2 is reiteration to determine aileron deflection given aileron span b2-b1

print "Type 1: optimisation to determine aileron span b2-b1 given b2 and aileron deflection."
print "Type 2: optimisation to determine aileron deflection given b2 and aileron span b2-b1."

type = input("1 or 2?: ")

b2 = input("Insert the outer aileron limit b2 (close to wing tip): ")

if type == 1:  # determine b2-b1 given b2 and aileron deflection

    d_aileron = input("Input aileron deflection (da): ")
    b_interval_check = -0.05  # Initially the inner wingspan is calculated reduced outer by 5 cm each time.
    b1 = b2
    i = 0
    while True: # The optimisation is done by a while loop checking through comparison to P_req
        i += 1
        b1 += b_interval_check
        P = P_val(b1, b2, d_aileron)

        # print P
        if (abs(P) - P_req > 0 and b_interval_check < 0) or (abs(P) - P_req < 0 and b_interval_check > 0):
            b_interval_check /= -1.5

        if abs(abs(P) - P_req) < 0.1:
            break

    print "Minimum required aileron limits:"
    print "\tb1: " + str(int(100*b1/b_span)) + "% (" + str(round(b1, 2)) + " m)"
    print "\tb2: " + str(int(100 * b2 / b_span)) + "% (" + str(round(b2, 2)) + " m)"
    print "\tLength per side: " + str(round((b2-b1)/2, 2)) + " m"
    print "\nConverged in " + str(i) + " iterations"

elif type == 2:

    aileron_span = input("Input aileron span b2-b1: ")
    d_aileron_check = 1  # Initial aileron deflection condition
    b1 = b2 - aileron_span

    while fabs(P) < P_req:

        d_aileron = d_aileron + d_aileron_check  # Change in aileron deflection each loop for optimisation
        P = P_val(b1, b2, d_aileron)

        print P

        if fabs(P) > P_req and d_aileron_check == 1: # Catch the 5 cm interval, go back to previous value, and proceed with 1 cm intervals.

            d_aileron = d_aileron - 1
            d_aileron_check = 0.1

    print "The required aileron deflection is: ", d_aileron, " [deg]"

